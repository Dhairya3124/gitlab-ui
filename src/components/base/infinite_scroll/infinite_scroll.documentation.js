import description from './infinite_scroll.md';

export default {
  followsDesignSystem: true,
  description,
};
